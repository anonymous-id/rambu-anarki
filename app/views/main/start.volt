<div class="inner cover">
    <h1 class="cover-heading">BERSIHKAN</h1>
    <p class="lead">orang yang kita sayangi dari <abbr title="Perusak">{{ linkTo("http://id.wikipedia.org/wiki/Malware", "Malware", false) }}</abbr>,
        <abbr title="Pengintai">{{ linkTo("http://id.wikipedia.org/wiki/Spyware", "Spyware", false) }}</abbr> dan <del>BEJAT MORAL</del>.</p>
    <form class="form-inline lead" role="form" method="post" id="web-form">
        <div class="form-group">
            <label class="sr-only" for="submit-url">Alamat situs</label>
            <input type="url" class="form-control" id="submit-url" placeholder="Alamat situs" required="required" autocomplete="off" autofocus="true">
        </div>
        <div class="form-group hide">
            <label class="sr-only" for="report-email">Alamat email</label>
            <input type="email" class="form-control" id="report-email" placeholder="Alamat email">
        </div>
        <button type="submit" class="btn btn-default">Lakukan</button>
    </form>
    <small>{{ linkTo(["#", "Pantau kemajuan tindakan", "id": "submit-report"]) }}</small>
</div>
