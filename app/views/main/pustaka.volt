<div class="row">
    <div class="col-md-9">
        <table class="table">
            <thead>
            <tr>
                <th>Nama</th>
                <th>Web</th>
                <th>Contact</th>
                <th>Server</th>
            </tr>
            </thead>
            <tbody>
            {% for dns in pustaka %}
                <tr>
                    <td>{{ dns.nama }}</td>
                    <td>{{ dns.web }}</td>
                    <td>{{ dns.email }}</td>
                    <td>{{ dns.server }}</td>
                </tr>
            {% endfor %}
            </tbody>
        </table>
    </div>
    <div class="col-md-3">
        <div class="badge badge-danger" style="padding: 2em">
            1001 Situs terdaptar
        </div>
    </div>
</div>