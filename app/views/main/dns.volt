<div class="inner cover">
    <h2 class="cover-heading">Daftarkan DNS</h2>
    <p class="lead">Informasi yang lengkap membuat moderasi lebih cepat.</p>
    <form role="form" method="post" id="dns-form">
        <div class="form-group">
            <label class="sr-only" for="dns-provider-name">Nama provider</label>
            <input type="text" name="nama" class="form-control" id="dns-provider-name" placeholder="Nama provider">
        </div>
        <div class="form-group">
            <label class="sr-only" for="dns-provider-page">Website provider</label>
            <input type="text" name="web" class="form-control" id="dns-provider-page" placeholder="Website provider">
        </div>
        <div class="form-group">
            <label class="sr-only" for="dns-provider-email">Email provider</label>
            <input type="email" name="email" class="form-control" id="dns-provider-email" placeholder="Email provider">
        </div>
        <div class="form-group">
            <label class="sr-only" for="dns-ip">Alamat IP Server</label>
            <input type="text" name="server" class="form-control" id="dns-ip" placeholder="Alamat IP DNS dipisahkan koma/spasi">
        </div>
        <button type="submit" class="btn btn-default">Daftarkan</button>
    </form>
</div>