<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Anonymous Indonesia">

    {{ getTitle() }}

    {{ assets.get("mainCss") and assets.outputCss("mainCss") }}

    {{ assets.get("pageCss") and assets.outputCss("pageCss") }}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

{{ content() }}
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
{{ assets.get("mainJs") and assets.outputJs("mainJs") }}

{{ assets.get("pageJs") and assets.outputJs("pageJs") }}

</body>
</html>
