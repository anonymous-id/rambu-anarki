<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    {#<h3 class="masthead-brand">R.A</h3>#}
                    <ul class="nav masthead-nav">
                        {% for nav, url in ["Beranda": 'start', "DNS": "dns" , "Pustaka": "pustaka", "Tentang": "tentang"] %}
                            {% if dispatcher.getActionName() === url  %}
                                <li class="active">
                            {% else %}
                                <li>
                            {% endif %}
                            {{ linkTo([url, nav]) }}
                            </li>
                        {% endfor %}
                    </ul>
                </div>
            </div>

            {{ content() }}

            <div class="mastfoot">
                <div class="inner">
                    <p>With respect to Anonymous Indonesia.</p>
                </div>
            </div>

        </div>

    </div>

</div>