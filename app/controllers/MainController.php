<?php

class MainController extends BaseController
{

    public function startAction()
    {

    }

    public function dnsAction()
    {
        if ($this->request->isPost() and $this->request->isAjax()) {
            if (count($this->request->getPost()) < 1) {
                return $this->response->setContentType('application/json')
                    ->setJsonContent([
                        'error'   => true,
                        'message' => "Salah satu informasi dibutuhkan"
                    ]);
            }
            $dns = new Dns();
            $success = $dns->create($this->request->getPost(), ['email', 'nama', 'web', 'server']);
            return $this->response->setContentType('application/json')
                ->setJsonContent([
                    "error" => !$success,
                    "message" => "Terimakasih atas partisipasi anda"
                ]);
        }

    }

    public function pustakaAction()
    {
        $this->view->pustaka = Dns::find();

    }

    public function tentangAction()
    {

    }

}

