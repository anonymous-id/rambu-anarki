<?php

use Phalcon\Mvc\Controller;

class BaseController extends Controller
{
    /**
     * <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

     */

    public function initialize()
    {
        $this->assets->collection('mainCss')
            ->addCss('assets/css/bootstrap-cosmo.css');
        $this->assets->collection('pageCss')
            ->addCss('assets/css/cover.css');
        $this->assets->collection('mainJs')
            ->addJs("assets/js/jquery-1.11.1.js")
            ->addJs("assets/js/jquery-migrate-1.2.1.js")
            ->addJs("assets/js/bootstrap.js")
            ->addJs("assets/js/ie10-viewport-bug-workaround.js");
        $this->assets->collection('pageJs')
            ->addJs('assets/js/form.js');

    }

}
