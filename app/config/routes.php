<?php
return [
    "GET" => [
        "start" => "main/start",
        "dns" => "main/dns",
        "pustaka" => "main/pustaka",
        "tentang" => "main/tentang",
    ],
    "POST" => [
        "start" => "main/start",
        "dns" => "main/dns",
        "pustaka" => "main/pustaka",
        "tentang" => "main/tentang",
    ]
];