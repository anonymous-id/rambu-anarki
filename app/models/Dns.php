<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class Dns extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $nama;

    /**
     *
     * @var string
     */
    public $web;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $server;

    /**
     *
     * @var integer
     */
    public $dilaporkan;

    /**
     *
     * @var integer
     */
    public $terdaftar;

    /**
     *
     * @var integer
     */
    public $diperbarui;

    public function beforeCreate()
    {
        $this->terdaftar = time();
    }

    public function beforeUpdate()
    {
        $this->diperbarui = time();
    }
}
