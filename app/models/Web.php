<?php

class Web extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $nama;

    /**
     *
     * @var string
     */
    public $alamat;

    /**
     *
     * @var integer
     */
    public $terdaftar;

    /**
     *
     * @var integer
     */
    public $diperbarui;

    /**
     *
     * @var integer
     */
    public $ip_pendaftar;

}
